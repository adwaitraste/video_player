from tkinter import *
import datetime
import tkinter as tk
from tkinter import filedialog
from tkinter import messagebox
from tkVideoPlayer import TkinterVideo
from tkstylesheet import TkThemeLoader

root = tk.Tk()
root.title("Video Player")
root.geometry("800x700+290+10")

lower_frame = tk.Frame(root, bg="#FFFFFF")
lower_frame.pack(fill="both", side=BOTTOM)

# Theme
theme = TkThemeLoader(root)
theme.loadStyleSheet("theme.tkss")

## Asset Loading
back_btn_image = PhotoImage(file="./Assets/back_button.png")
forward_btn_image = PhotoImage(file="./Assets/forward_button.png")
play_btn_image = PhotoImage(file="./Assets/play_button.png")
pause_btn_image = PhotoImage(file="./Assets/pause_button.png")
stop_btn_image = PhotoImage(file="./Assets/stop_button.png")

is_loaded = tk.BooleanVar(value=False)


def changeOnHover(button, colorOnHover, colorOnLeave):
  
    # adjusting backgroung of the widget
    button.bind("<Enter>", func=lambda e: button.config(
        background=colorOnHover))
  
    button.bind("<Leave>", func=lambda e: button.config(
        background=colorOnLeave))

## Video Player Control Functions
def change_duration(event):
    duration = my_player.video_info()["duration"]
    end_time["text"] = str(datetime.timedelta(seconds=duration))
    slider["to"] = duration

def change_scale(event):
    slider_val.set(my_player.current_duration())

def select_video():
    file_path = filedialog.askopenfilename()
    if file_path.lower().endswith('.mp4'):
        is_loaded.set(True)
        my_player.load(file_path)
        slider.config(to=0, from_=0)
        play_btn["text"] = "Play"
        slider_val.set(0)
        messagebox.showinfo("Video Loaded", "Please click the play button to watch the video!")
    else:
        is_loaded.set(False)
        messagebox.showerror("Error", "Unsupported File Type")
    
def seek(value):
    my_player.seek(int(value))

def jump(value:int):
    my_player.seek(int(slider.get()) + value)
    slider_val.set(slider.get() + value)

def toggle_play_pause():
    if is_loaded.get():
        if my_player.is_paused():
            my_player.play()
            play_btn["image"] = pause_btn_image
        else:
            my_player.pause()
            play_btn["image"] = play_btn_image
    else:
        messagebox.showerror("Error", "No Video Loaded")

def stop():
    my_player.stop()
    play_btn["image"] = play_btn_image

def finish_video(event):
    slider.set(slider["to"])
    play_btn["text"] = "Play"
    slider.set(0)

## Browse Button 
load_btn = tk.Button(root, text="Browse", bg="yellow", font=("calibri", 12, "bold"), command=select_video)
load_btn.pack(ipadx=12, ipady=4, anchor=tk.CENTER)
changeOnHover(load_btn, "red", "yellow")


## Video Player
my_player = TkinterVideo(root, scaled=True)
my_player.pack(expand=True, fill="both")
my_player.bind("<<Duration>>", change_duration)
my_player.bind("<<SecondChanged>>", change_scale)
my_player.bind("<<Ended>>", finish_video)

## Progress Bar Section
start_time = tk.Label(root, text=str(datetime.timedelta(seconds=0)))
start_time.pack(side=LEFT)

slider_val = tk.IntVar(root)
slider = tk.Scale(root, variable=slider_val, from_=0, to=0, orient=HORIZONTAL, command=seek)
slider.pack(side=LEFT, fill=X, expand=True)

end_time = tk.Label(root, text=str(datetime.timedelta(seconds=0)))
end_time.pack(side=LEFT)

## Control Buttons Section
back_btn = tk.Button(lower_frame, image=back_btn_image, width=10, height=50, command=lambda:jump(-5))
back_btn.pack(expand=True, fill="both", side=LEFT)

play_btn = tk.Button(lower_frame, image=play_btn_image, width=50, height=50, command=toggle_play_pause)
play_btn.pack(expand=True, fill="both", side=LEFT)

stop_btn = tk.Button(lower_frame, image=stop_btn_image, width=50, height=50, command=stop)
stop_btn.pack(expand=True, fill="both", side=LEFT)

forward_btn = tk.Button(lower_frame, image=forward_btn_image, width=10, height=50, command=lambda:jump(5))
forward_btn.pack(expand=True, fill="both", side=LEFT)

root.mainloop()